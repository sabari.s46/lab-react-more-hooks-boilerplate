import React from "react";
import "../App.css";

export default function Divcontainer({ value, visible, toggle }) {
  return (
    <div className="boss">
      <div className="todo-box">
        <div className="todo">{visible ? value : "This content is hidden"}</div>
        <button className="toggle-button" onClick={toggle}>
          Toggle
        </button>
      </div>
    </div>
  );
}
