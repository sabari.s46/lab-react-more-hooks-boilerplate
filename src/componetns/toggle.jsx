import React, { useRef, useReducer } from "react";
import Divcontainer from "./divcontainer";
import "../App.css";

const ACTIONS = {
  ADD_LIST: "addList",
  UPDATE_VALUE: "updateValue",
  TOGGLE_CONTEXT: "toggleContext",
};

function reducer(state, action) {
  switch (action.type) {
    case ACTIONS.ADD_LIST:
      const newItem = {
        id: Date.now(),
        text: state.value,
        visible: true,
      };
      return {
        ...state,
        DisplayList: state.DisplayList.concat(newItem),
        value: "",
      };

    case ACTIONS.UPDATE_VALUE:
      return { ...state, value: action.payload };

    case ACTIONS.TOGGLE_CONTEXT:
      return {
        ...state,
        DisplayList: state.DisplayList.map((item) =>
          item.id === action.payload
            ? { ...item, visible: !item.visible }
            : item
        ),
      };

    default:
      return state;
  }
}

function Toggle() {
  const inputElement = useRef();
  const [state, dispatch] = useReducer(reducer, {
    DisplayList: [],
    value: "",
  });

  function handleEnter(event) {
    if (event.key === "Enter" && state.value.trim() !== "") {
      dispatch({ type: ACTIONS.ADD_LIST });
    }
  }

  const handleToggle = (id) => {
    dispatch({ type: ACTIONS.TOGGLE_CONTEXT, payload: id });
  };

  const focusInput = () => {
    inputElement.current.focus();
  };

  return (
    <div>
      <div className="TextBox">
        <input
          type="text"
          value={state.value}
          onChange={(e) =>
            dispatch({ type: ACTIONS.UPDATE_VALUE, payload: e.target.value })
          }
          placeholder="Type here"
          onKeyDown={handleEnter}
          ref={inputElement}
        />
      </div>
      {state.DisplayList.map((item) => (
        <Divcontainer
          key={item.id}
          value={item.text}
          visible={item.visible}
          toggle={() => handleToggle(item.id)}
        />
      ))}
      <div className="button">
        <button onClick={focusInput}>Get back writing</button>
      </div>
    </div>
  );
}

export default Toggle;
